﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{
    class Obstacle
    {
        private Vector3 m_position;
        private Texture2D m_texture;
        private Model m_obstacle;
        public float m_landRotateRate = 1.0f;
        public float m_landRotation = 0.0f; //rotation in radians
        private float speed = 100;
        public BasicEffect m_basicEffect;
        public Vector3 m_cameraPosition;

        public Obstacle(Vector3 position, Texture2D texture)
        {
            m_position = position;
            m_texture = texture;
        }
        public Obstacle(string modelLink, Game game)
        {
            m_obstacle = game.Content.Load<Model>(modelLink);

            m_basicEffect = m_obstacle.Meshes[0].MeshParts[0].Effect as BasicEffect;
            m_cameraPosition = new Vector3(0, 0, -10f);
        }
        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                m_landRotation += MathHelper.ToRadians(m_landRotateRate) *
                                  (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                if (m_landRotation > MathHelper.TwoPi)
                {
                    m_landRotation -= MathHelper.TwoPi;
                }
            }
        }

        public void Draw(GameTime gameTime, Camera camera)

        {
            float cof_y=2.5f, cof_z=2.5f;
            if (MathHelper.ToDegrees(m_landRotation) >= 90 && MathHelper.ToDegrees(m_landRotation) < 180)
            {
                cof_z = -cof_z;
            }
            if (MathHelper.ToDegrees(m_landRotation) >= 180 && MathHelper.ToDegrees(m_landRotation) < 270)
            {
                cof_z = -cof_z;
                cof_y = -cof_y;
            }
            if (MathHelper.ToDegrees(m_landRotation) >= 270 && MathHelper.ToDegrees(m_landRotation) < 360)
            {
                cof_y = -cof_y;
            }
            
            //Matrix landTransform = Matrix.CreateRotationX(m_landRotation);
            Matrix landTransform = Matrix.CreateTranslation(0, -2.65f, 7.8f) * 
                                   Matrix.CreateScale(0.5f, 0.5f, 0.5f)*
                                   Matrix.CreateTranslation(0, (float)Math.Cos(m_landRotation)+cof_y, (float)Math.Sin(m_landRotation)-cof_z);
            Matrix orbit = Matrix.CreateFromAxisAngle(Vector3.Zero, 0.5f);
            // *
                                   //Matrix.CreateTranslation(0, -2.65f, 7.8f) *
                                  // Matrix.CreateScale(0.1f, 0.1f, 0.1f);
            //Matrix landTransform = Matrix.CreateRotationX(m_landRotation) *
            //                       Matrix.CreateTranslation(0, -2.65f, 7.8f) *
            //                       Matrix.CreateScale(0.1f, 0.1f, 0.1f);
            m_obstacle.Draw(landTransform, camera.View, camera.Projection);
        }

    }

    class Tree : Obstacle
    {
        private bool m_isCollision = false;
        private bool m_isFriendly = false;
        public Tree(Vector3 position, Texture2D texture) : base(position, texture)
        {

        }

        public void Collision()
        {
            if (m_isCollision)
            {
                //TODO: Handle collision with non-friendly obstacle.
            }
        }
    }

    class GarbageCan : Obstacle
    {
        private bool m_isCollision = false;
        private bool m_isFriendly = true;
        public GarbageCan(Vector3 position, Texture2D texture) : base(position, texture)
        {

        }

        public void Collision()
        {
            if (m_isCollision)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    //TODO: Collision logic for garbage can
                }
            }
        }
    }
}
