﻿using System;
using System.CodeDom;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Scrolling scrolling1;
        private Scrolling scrolling2;
        private Sky sky;

        private Vector3 camTarget;
        private Vector3 camPosition;
        private Matrix projectMatrix, viewMatrix, worldMatrix;
        BasicEffect orangeEffect;

        private Vector3 ambientLight, diffuseLight, specularLight, lightPosition;
        Vector3 ambientMaterial;
        Vector3 diffuseMaterial;
        Vector3 specularMaterial;
 
        public Model model;

        private Land land;
        private Obstacle obstacle;

        //Orbit
        private bool orbit;
        private Camera camera;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            camera = new PerspectiveCamera(this);
            
            camera.CameraPosition = new Vector3(Vector2.Zero,10.0f);
            Components.Add(camera);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 500;
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            graphics.PreferMultiSampling = true;
            GraphicsDevice.PresentationParameters.MultiSampleCount = 8;
            graphics.ApplyChanges();
            base.Initialize();


            //SetupCamera

            camTarget = new Vector3(0, 0, 0);
            camPosition = new Vector3(0, 0, -10f);

            projectMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45f),
                GraphicsDevice.DisplayMode.AspectRatio, 1f, 1000f);
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget, Vector3.Up);
            worldMatrix = Matrix.CreateWorld(camTarget, Vector3.Forward, Vector3.Up);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            //model = Content.Load<Model>("Models\\Orange");
            //orangeEffect = model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            land = new Land("models\\Earth",this);
            obstacle = new Obstacle("models\\Earth",this);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.C))
            {
                camera.CameraPosition = new Vector3(0,5.0f,-1.0f);
            }
            land.Update(gameTime);
            obstacle.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.SkyBlue);
            //land.Draw(gameTime, camera);
            obstacle.Draw(gameTime,camera);
            base.Draw(gameTime);
        }
    }
}
