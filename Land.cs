﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{
    public class Land
    {
        public Model m_land;
        public BasicEffect m_basicEffect;
        public Vector3 m_cameraPosition;
        public float m_landRotateRate = 1.0f;
        public float m_landRotation = 0.0f; //rotation in radians
        private float speed = 100;
        public RasterizerState wireframeRaster, solidRaster;
        public Land(string modelLink, Game game)
        {
            m_land = game.Content.Load<Model>(modelLink);

            m_basicEffect = m_land.Meshes[0].MeshParts[0].Effect as BasicEffect;
            m_cameraPosition = new Vector3(0, 0, -10f);
        }

        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                m_landRotation += MathHelper.ToRadians(m_landRotateRate) *
                                  (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                if (m_landRotation > MathHelper.TwoPi)
                {
                    m_landRotation -= MathHelper.TwoPi;
                }
            }
        }

        public void Draw(GameTime gameTime, Camera camera)
        {
            Matrix landTransform = Matrix.CreateRotationZ(MathHelper.ToRadians(90)) *
                                   Matrix.CreateRotationX(m_landRotation) *
                                   Matrix.CreateTranslation(0, -2.65f, 7.8f) *
                                   Matrix.CreateScale(2.5f, 1f, 1f);
            m_land.Draw(landTransform, camera.View, camera.Projection);
        }

    }
}
