﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XELibrary;

namespace GarbageDog
{
    class Sun
    {
        private int m_speed = 25;
        public Vector3 m_cameraPosition;
        private float m_horizontalPosition;
        private int MAX_LEFT = -4;
        private int MAX_RIGHT = 4;
        private Model m_sun;
        private bool isWalking;
        private float m_sunRotate = 0;
        private float m_sunPulse = 0;
        private long m_oldTime = 0, new_time = 0;
        private bool m_isSunPulseForwars = true;
        public Sun(Game game, string sun)
        {
            m_sun = game.Content.Load<Model>(sun);
        }

        public void Update(GameTime gameTime)
        {
            m_oldTime = gameTime.TotalGameTime.Milliseconds;
            if (Math.Abs(m_oldTime - new_time) > 100)
            {
                m_sunRotate = m_sunRotate == 360 ? 0 : m_sunRotate + 5f;
                if (m_isSunPulseForwars)
                {
                    m_sunPulse += 0.075f;
                    if (m_sunPulse > 1.15)
                    {
                        m_isSunPulseForwars = false;
                    }
                }
                else if (!m_isSunPulseForwars)
                {
                    m_sunPulse -= 0.075f;
                    if (m_sunPulse < -1.15)
                    {
                        m_isSunPulseForwars = true;
                    }
                }
                new_time = m_oldTime;

            }
        }

        public void Draw(GameTime gameTime, Camera camera, GraphicsDevice graphicsDevice)
        {
            Matrix sunRotate =
                Matrix.CreateRotationZ(MathHelper.ToRadians(0)) *
                Matrix.CreateTranslation(1.5f, 2.5f, 10);

            m_sun.Draw(sunRotate, camera.View, camera.Projection);

        }

    }
}
