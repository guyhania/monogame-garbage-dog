﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarbageDog.Auxiliary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;


namespace GarbageDog
{
    class Obstacle
    {
        private const float TWO_PI_DEG = 360;
        private const float TWO_PI_OVER_EIGHT_DEG = 45;
        private const float TWO_PI_OVER_FOUR_DEG = 90;

        private Model m_obstacle;
        public float m_landRotateRate = 1.0f;
        public float m_landRotation = 0.0f; //rotation in radians
        private float speed = 25;
        public BasicEffect m_basicEffect;
        public Vector3 m_cameraPosition;
        private float m_horizontalPos;
        public ObstacleTypes.ObstacleType Type { get; set; }
        public float Delay { get; set; }
        public float HorizontalPos { get; set; }

        private AuxillaryFunctions aux = AuxillaryFunctions.Functions();
        private float m_yRotetion = 0.0f;

        //Collision detection
        private Vector3 minCorner;
        private Vector3 maxCorner;
        public BoundingBox box;
        float M41;
        float M42;
        float M43;

        public Obstacle() { }

        public void Initialize(Game game)
        {
            m_obstacle = game.Content.Load<Model>(aux.ParseTypeToLink(Type.ToString()));
            //Collision detection
            ObjectsConfiguration objConfig = ObjectsConfigurations.GetObjectConfiguration(Type);
            minCorner = (new Vector3(M41, M42, M43));
            maxCorner = (new Vector3(M41, M42, M43));
            box = new BoundingBox(minCorner, maxCorner);
        }
        public Obstacle(string Type, string Delay, string HorizontalPos, string isCollidable, Game game)
        {
            m_obstacle = game.Content.Load<Model>(aux.ParseTypeToLink(Type));
            aux.MyParseFloat(Delay);
            m_horizontalPos = aux.MyParseFloat(HorizontalPos);
            aux.ParseIsCollidable(isCollidable);

        }

        public Obstacle(string modelLink, Game game, GraphicsDevice graphicsDevice, float delay, float horizontalPos, bool isModel)
        {
            if (isModel)
            {
                m_obstacle = game.Content.Load<Model>(modelLink);
                m_basicEffect = m_obstacle.Meshes[0].MeshParts[0].Effect as BasicEffect;

                m_cameraPosition = new Vector3(0, 0, -10f);
                m_horizontalPos = horizontalPos;
            }

        }
        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            ObjectsConfiguration objConfig = ObjectsConfigurations.GetObjectConfiguration(Type);

            if (keyboardState.IsKeyDown(Keys.Down))
            {
                m_landRotation += MathHelper.ToRadians(-m_landRotateRate / (objConfig.m_magnification * 0.75f * 0 + 1)) *
                                  (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
            }
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                m_landRotation += MathHelper.ToRadians(m_landRotateRate / (objConfig.m_magnification * 0.75f * 0 + 1)) *
                                  (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
            }
        }

        public void Draw(GameTime gameTime, Camera camera, GraphicsDevice graphicsDevice)
        {
            float landRotationDeg = MathHelper.ToDegrees(m_landRotation);
            float absDelay = Math.Abs(Delay);
            if ((landRotationDeg - TWO_PI_OVER_FOUR_DEG - 30) < absDelay && absDelay < (landRotationDeg + TWO_PI_OVER_FOUR_DEG))
            {
                ObjectsConfiguration objConfig = ObjectsConfigurations.GetObjectConfiguration(Type);
                Matrix landTransform = Matrix.CreateScale(objConfig.m_magnification, objConfig.m_magnification, 1) *
                                       Matrix.CreateRotationX(MathHelper.ToRadians(objConfig.m_xRotation)) *
                                       Matrix.CreateRotationY(m_yRotetion - objConfig.m_yRotation) *
                                       Matrix.CreateRotationZ(MathHelper.ToRadians(objConfig.m_zRotation));
                landTransform *= Matrix.CreateTranslation(m_horizontalPos,
                                     (float)(Math.Cos(m_landRotation + MathHelper.ToRadians(Delay)) * 2.7),
                                     (float)(Math.Sin(m_landRotation + MathHelper.ToRadians(Delay)) * 2.9)) *
                                 Matrix.CreateTranslation(HorizontalPos, objConfig.m_yValue, 7.8f);
                graphicsDevice.DepthStencilState = DepthStencilState.Default;
                graphicsDevice.BlendState = BlendState.AlphaBlend;

                m_obstacle.Draw(landTransform, camera.View, camera.Projection);
                M41 = landTransform.M41;
                M42 = landTransform.M42;
                M43 = landTransform.M43;

                updateBounding(new Vector3(M41, M42 - 1, M43 - 1), new Vector3(M41, M42 + 1, (float)(M43 + 1.6))); //poo,rock,flea,cone

                m_obstacle.Draw(landTransform, camera.View, camera.Projection);
            }
        }

        public String getObsBox()
        {
            return box.ToString();
        }

        public void updateBounding(Vector3 min, Vector3 max)
        {
            box.Min = min;
            box.Max = max;
        }
        public String showBounding()
        {
            return "Type:" + Type + " Cord:" + box.ToString();
        }
    
    }
}
