﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{
    class Dog
    {
        private int m_speed = 25;
        public Vector3 m_cameraPosition;
        private float m_horizontalPosition;
        private int MAX_LEFT = -4;
        private int MAX_RIGHT = 4;
        private Model m_dog;
        private bool isWalking;
        private float m_walkDeg = 0;
        private long m_oldTime = 0, new_time = 0;
        private List<SoundEffect> soundEffects;
        
        private int health;    //Dog's health
                               
        public HealthBar healthBar;  //Health obj

        //Collision detection
        private Vector3 minCorner;
        private Vector3 maxCorner;
        private BoundingBox box;

        public Dog(Game game, string dog)
        {

            m_dog = game.Content.Load<Model>(dog);
            m_horizontalPosition = 0;
            isWalking = false;
            health = 540;
            soundEffects = new List<SoundEffect>();
            soundEffects.Add(game.Content.Load<SoundEffect>(@"Sounds/DogRunsFastBreathe"));
            soundEffects.Add(game.Content.Load<SoundEffect>(@"Sounds/DogStandsHaveyBreathe"));

            healthBar = new HealthBar(game); //Initiate health bar
           
            /*Collision detection
             * minCorner is the bottom corner of the object
             * maxCorner is the topt corner of the object
             */

            minCorner = (new Vector3(-1f, -2.18f, 13f));
            maxCorner = (new Vector3(1f, -0.18f, 13f));
            box = new BoundingBox(minCorner, maxCorner);
            box.ToString(); ///check it!!!
        }


        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            healthBar.Update(health); //Updates dog's health bar

            if (keyboardState.IsKeyDown(Keys.Up))
            {
                if (keyboardState.IsKeyDown(Keys.Right))
                {
                    if (m_horizontalPosition < 4)
                    {
                        m_horizontalPosition += 0.105f;
                        //Update Bounding Box
                        box = new BoundingBox(box.Min + new Vector3(0.105f, 0, 0), box.Max + new Vector3(0.105f, 0, 0));
                    }
                }

                if (keyboardState.IsKeyDown(Keys.Left))
                {
                    if (m_horizontalPosition > -4)
                    {
                        m_horizontalPosition -= 0.105f;
                        //Update Bounding Box
                        box = new BoundingBox(box.Min - new Vector3(0.105f, 0, 0), box.Max - new Vector3(0.105f, 0, 0));
                    }
                }

                {
                    if (!isWalking)
                    {
                        soundEffects[0].CreateInstance().Play();
                        soundEffects[0].CreateInstance().IsLooped=true;
                        soundEffects[1].CreateInstance().Stop();;
                    }
                    isWalking = true;
                    m_oldTime = gameTime.TotalGameTime.Milliseconds;
                    if (Math.Abs(m_oldTime - new_time) > 5)
                    {
                        m_walkDeg = m_walkDeg == 360 ? 0 : m_walkDeg + 5f;
                        new_time = m_oldTime;
                    }
                }
            }
            else
            {

                if (isWalking)
                {
                    soundEffects[0].CreateInstance().Stop();
                    soundEffects[1].CreateInstance().Play();
                    soundEffects[1].CreateInstance().IsLooped=true;

                }

                isWalking = false;
            }

        }

        public void Draw(GameTime gameTime, Camera camera, GraphicsDevice graphicsDevice,SpriteBatch spriteBatch)
        {
            healthBar.Draw(gameTime, graphicsDevice, spriteBatch); //Draw health bar
            Matrix dogTransform = Matrix.CreateScale(1.35f, 1.35f, 1) *
                Matrix.CreateTranslation(m_horizontalPosition, -1.18f, 13);
            if (isWalking)
            {
                dogTransform *= Matrix.CreateTranslation(0, (float)Math.Abs(Math.Sin(MathHelper.ToRadians(m_walkDeg))) * 0.10f, 0);
            }
            graphicsDevice.DepthStencilState = DepthStencilState.Default;
            graphicsDevice.BlendState = BlendState.AlphaBlend;

            m_dog.Draw(dogTransform, camera.View, camera.Projection);
            upadateBoundingbox(dogTransform.M41, dogTransform.M42, dogTransform.M43); //
            
        }

        /*Return the box corners as String*/
        public String getBoxLocation()
        {
            return box.ToString();
        }

        /*Return the bounding box object*/
        public BoundingBox getDogBox()
        {
            return box;
        }

        private void upadateBoundingbox(float m41, float m42, float m43)
        {
            box = new BoundingBox(new Vector3(m41 - 1, m42 - 1, m43 - 1), new Vector3(m41 + 1, m42 + 1, m43 + 1));
        }

        public void setHelath(int newHealth)
        {
            health = newHealth;
        }

        public int getHelath()
        {
            return health;
        }
    }

}
