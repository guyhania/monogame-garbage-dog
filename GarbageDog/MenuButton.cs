﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using GarbageDog.Auxiliary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;


namespace GarbageDog
{
    class MenuButton
    {
        private readonly int BTN_PLAY_POS_Y = 650;
        private readonly int BTN_PLAY_POS_X = 50;
        private readonly int BTN_EXIT_POS_Y = 860;
        private readonly int BTN_EXIT_POS_X = 130;

        public enum ButtonType
        {
            Play,
            Exit
        }

        private Texture2D m_buttonTextureReleased;
        private Texture2D m_buttonTexturePressed;
        private Texture2D m_buttonTexture;
        private ButtonType m_buttonType;
        private int m_positionX, m_positionY;
        private bool isPressed = false;
        public MenuButton(Game game, ButtonType buttonType)
        {
            string root = AuxillaryFunctions.Functions().ContentPathCreator();

            switch (buttonType)
            {
                case ButtonType.Play:
                    m_buttonTextureReleased = game.Content.Load<Texture2D>(root + "\\Menu\\Buttons\\Play\\punpressed");
                    m_buttonTexture = game.Content.Load<Texture2D>(root + "\\Menu\\Buttons\\Play\\punpressed");
                    m_buttonTexturePressed = game.Content.Load<Texture2D>(root + "\\Menu\\Buttons\\Play\\ppressed");
                    m_positionY = BTN_PLAY_POS_Y;
                    m_positionX = BTN_PLAY_POS_X;

                    break;
                case ButtonType.Exit:
                    m_buttonTextureReleased = game.Content.Load<Texture2D>(root + "\\Menu\\Buttons\\Exit\\punpressed");
                    m_buttonTexture = game.Content.Load<Texture2D>(root + "\\Menu\\Buttons\\Exit\\punpressed");
                    m_buttonTexturePressed = game.Content.Load<Texture2D>(root + "\\Menu\\Buttons\\Exit\\ppressed");
                    m_positionY = BTN_EXIT_POS_Y;
                    m_positionX = BTN_EXIT_POS_X;

                    break;
            }
            m_buttonType = buttonType;
        }

        private bool HitCheck()
        {
            MouseState mState = Mouse.GetState();
            if (mState.X > (m_positionX) &&
                mState.X < (m_positionX + m_buttonTextureReleased.Width) &&
                mState.Y > (m_positionY) &&
                mState.Y < (m_positionY + m_buttonTextureReleased.Height))
            {
                return true;
            }

            return false;
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            if (HitCheck())
            {
                m_buttonTexture = m_buttonTexturePressed;
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    isPressed = true;
                }
            }
            else
            {
                m_buttonTexture = m_buttonTextureReleased;
            }
        }

        public bool IsPressed()
        {
            return isPressed;
        }

        public void ReleaseButton()
        {
            isPressed = false;
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDeviceManager graphics)
        {

            spriteBatch.Draw(m_buttonTexture,
                new Rectangle(m_positionX, m_positionY, m_buttonTexture.Width, m_buttonTexture.Height),
                null,
                Color.White,
                0,
                Vector2.Zero,
                SpriteEffects.None,
                0);
        }
    }
}
