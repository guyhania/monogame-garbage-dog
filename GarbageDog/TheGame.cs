﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using GarbageDog.Auxiliary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{
    public class TheGame
    {
        private bool isLoaded = false;

        private Vector3 camTarget, camPosition;
        private Matrix projectMatrix, viewMatrix, worldMatrix;
        private bool m_firstShake = true;


        private Land land;
        private List<Obstacle> obstacles;
        private Dog dog;
        private Sun sun;

        private Camera camera;

        //Show text  
        private SpriteFont font;
        
        

        //Origin and collision Bounding box
        String boxLocation;
        String obsLocation;

        //Collision Text
        private String collisionText = "Collision!";
        private string noCollisionText = "No Collision";

        //Collision
        bool isCollied = false;
        String lastCollision;

        //Game OVer
        String gameOverText = "GAME OVER";
        bool gameOverCheck = false;


        public TheGame(Game game, GraphicsDeviceManager graphics)
        {

            camera = new PerspectiveCamera(game);

            camera.CameraPosition = new Vector3(0, 3.0f, 17.2f);
            game.Components.Add(camera);
            game.Content.RootDirectory = "Content";
        }

        public void Initialize(Game game, GraphicsDeviceManager graphics)
        {
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.ApplyChanges();
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.ApplyChanges();
            graphics.PreferMultiSampling = true;
            game.GraphicsDevice.PresentationParameters.MultiSampleCount = 8;
            graphics.ApplyChanges();

            lastCollision = "none";
            //SetupCamera

            camTarget = new Vector3(0, 0, 0);
            camPosition = new Vector3(0, 0, -10f);

            projectMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45f),
                game.GraphicsDevice.DisplayMode.AspectRatio, 1f, 1000f);
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget, Vector3.Up);
            worldMatrix = Matrix.CreateWorld(camTarget, Vector3.Forward, Vector3.Up);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public void LoadContent(Game game)
        {

           font = game.Content.Load<SpriteFont>("galleryFont");


            dog = new Dog(game, @"player\dog");
            sun = new Sun(game, @"models\sun");
            land = new Land(@"models\Earth", game);
            string jsonMap = File.ReadAllText(AuxillaryFunctions.Functions().ContentPathCreator()+
                                              "..\\..\\..\\..\\..\\..\\..\\WorldMap.json",Encoding.UTF8);

            obstacles = new JavaScriptSerializer().Deserialize<List<Obstacle>>(jsonMap);

            obstacles = obstacles.OrderBy(x => x.Delay).ToList();
            foreach (Obstacle obstacle in obstacles)
            {
                obstacle.Initialize(game);
            }

            isLoaded = true;

        }

        public bool IsLoaded()
        {
            return isLoaded;
        }

      

        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            //Some bug with the camera, shaking it back and fourth to render the Dog anf the Sun
            if (m_firstShake)
            {
                camera.CameraPosition = new Vector3(0, 0, camera.CameraPosition.Z + 0.1f);
                camera.CameraPosition = new Vector3(0, 0, camera.CameraPosition.Z - 0.1f);
                m_firstShake = false;
            }

            land.Update(gameTime, keyboardState);

            // Checks for collisons
            isCollied = false;
            foreach (Obstacle element in obstacles)
            {
                element.Update(gameTime, keyboardState);

                String boundingtext = element.showBounding().ToString();

                if (!isCollied && lastCollision != element.Type.ToString())
                {
                    isCollied = checkCollision(element.box, element.Type.ToString(), boundingtext, gameTime);
                }

                obsLocation = element.showBounding();

            }

            dog.Update(gameTime, keyboardState);
            boxLocation = dog.getBoxLocation();
            sun.Update(gameTime);
        }

        

        public void Draw(GameTime gameTime, Game game, ref GraphicsDeviceManager graphics,SpriteBatch spriteBatch)
        {
            game.GraphicsDevice.Clear(Color.SkyBlue);
            land.Draw(gameTime, camera);
            foreach (Obstacle obstacle in obstacles)
            {
                obstacle.Draw(gameTime, camera, graphics.GraphicsDevice);
            }

            dog.Draw(gameTime, camera, graphics.GraphicsDevice,spriteBatch);
            sun.Draw(gameTime, camera, graphics.GraphicsDevice);
           // spriteBatch.Begin(0, null, null, null, null, null, null);
            //spriteBatch.DrawString(font,boxLocation, Vector2.Zero,Color.Red);
            //spriteBatch.DrawString(font, obsLocation, new Vector2(0, 50), Color.Red);
            //spriteBatch.DrawString(font, "Speed = " + land.getSpeed().ToString(), new Vector2(0, 200), Color.Red);
            //spriteBatch.DrawString(font, "Last Collision = " + lastCollision.ToString(), new Vector2(0, 250), Color.Red);

            //if (isCollied)
            //{
            //    spriteBatch.DrawString(font, collisionText, Vector2.Zero + new Vector2(100, 100), Color.Red);
            //}
            //if (!isCollied)
            //{
            //    spriteBatch.DrawString(font, noCollisionText, Vector2.Zero + new Vector2(100, 150), Color.Red);
           // }
            if (gameOverCheck)
            {
                spriteBatch.DrawString(font, gameOverText, Vector2.Zero + new Vector2(500, 500), Color.Red);
            }


           // spriteBatch.End();
        }
        /*Checks for collisions between obstacle and dog */
        protected bool checkCollision(BoundingBox obsBox, String type, string boundingtext, GameTime gameTime)
        {
            if ((obsBox.Max == Vector3.Zero && obsBox.Min == Vector3.Zero))
                return false;
            //System.Diagnostics.Debug.WriteLine("Checking collision for Obstacle: " + type +boundingtext);
            //System.Diagnostics.Debug.WriteLine("Checking collision for DOGYYYYY:             " + dog.getBoxLocation());

            if (dog.getDogBox().Intersects(obsBox))
            {
                //System.Diagnostics.Debug.WriteLine("COLLISION FOR: "+type+"  !!!!!!!!!!!  " + "Is Collide: "+isCollied.ToString());

                if (type == "Rock" || type == "Poo" || type == "Cone" || type == "StopSign" ||
                    type == "Hydrant" || type == "LightPole"  || type == "TrashCan")
                {
                    if (dog.getHelath() <= 4)
                    {
                        gameOverCheck = true;
                    }
                    //Lower dog's life by 60
                    dog.setHelath(dog.getHelath() - 180);
                    dog.healthBar.health = dog.healthBar.health - 180;
                }

                if (type == "FishBone" || type == "Bone")
                {
                    if (dog.getHelath() < 540)
                    {
                        dog.setHelath(dog.getHelath() + 180);
                        dog.healthBar.health = dog.healthBar.health + 180;
                    }
                  
                }


                if (type == "Flea")
                {
                    land.setSpeed(land.getSpeed() + 100);
                }

                if (type == "Tick")
                {
                    land.setSpeed(land.getSpeed() - 20);
                }
                lastCollision = type;
                return true;
            }
            return false;
        }


    }
}
