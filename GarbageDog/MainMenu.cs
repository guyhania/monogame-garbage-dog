﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{

    public class MainMenu
    {
        private MouseCustomCoursor m_coursor;
        private bool m_isPoused = true;
        private int m_currentMenuPosition = 0;
        public Texture2D m_background;
        public Texture2D m_title;
        private MenuButton m_playButton, m_exitButton;
        //private TheGame theGame;
        public Menu gameState { get; set; }
        private bool isGameLoaded = false;

        public MainMenu(Game game, GraphicsDeviceManager graphics)
        {

            m_background = game.Content.Load<Texture2D>(@"Menu\pScreen");
            m_title = game.Content.Load<Texture2D>(@"Menu\pTitle");
            m_playButton = new MenuButton(game, MenuButton.ButtonType.Play);
            m_exitButton = new MenuButton(game, MenuButton.ButtonType.Exit);
            gameState = Menu.MainMenu;
            m_coursor = new MouseCustomCoursor(game);
            // theGame = new TheGame(game, graphics);
            Initialize(game, graphics);

        }

        private void Initialize(Game game, GraphicsDeviceManager graphics)
        {
            //  theGame.Initialize(game, graphics);
        }

        public int GetMenuPosition()
        {
            return m_currentMenuPosition;
        }

        public void SetMenuPosition(int cuttentPosition)
        {
            m_currentMenuPosition = cuttentPosition;
        }


        public bool GetCurrentGameState()
        {
            return m_isPoused;
        }

        public void SetCurrentGameState()
        {
            m_isPoused = !m_isPoused;
        }

        public void ReleaseButtons()
        {
            m_playButton.ReleaseButton();
            m_exitButton.ReleaseButton();
        }



        public void Update(GameTime gameTime, Game game, KeyboardState keyboardState)
        {
            m_playButton.Update(gameTime);
            m_exitButton.Update(gameTime);
            if (m_playButton.IsPressed())
            {
                gameState = Menu.Game;
            }
            else if (m_exitButton.IsPressed() && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                game.Exit();

            }
        }

        public void Draw(GameTime gameTime, ref GraphicsDeviceManager graphics, SpriteBatch spriteBatch, Game game)
        {
            if (gameState == Menu.MainMenu)
            {
                spriteBatch.Draw(m_background,
                    new Rectangle(0, 0, graphics.GraphicsDevice.Viewport.Width,
                        graphics.GraphicsDevice.Viewport.Height),
                    null,
                    Color.White,
                    0,
                    Vector2.Zero,
                    SpriteEffects.None,
                    0);
                spriteBatch.Draw(m_title,
                    new Rectangle(0, 0, (int)(m_title.Width / 1.5f),
                        (int)(m_title.Height / 1.5f)),
                    null,
                    Color.White,
                    0,
                    new Vector2(-graphics.GraphicsDevice.Viewport.Width * 0.95f, -graphics.GraphicsDevice.Viewport.Height / 25),
                    SpriteEffects.None,
                    0);

                m_playButton.Draw(spriteBatch, graphics);
                m_exitButton.Draw(spriteBatch, graphics);
                m_coursor.Draw(spriteBatch, graphics);
            }

            if (gameState == Menu.Game)
            {
                isGameLoaded = true;
            }
        }

    }




    public enum Menu
    {
        MainMenu,
        Game,
    }

    enum GameState
    {
        Pouse,
        Play
    }
}
