﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using GarbageDog.Auxiliary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GarbageDog
{
    class MouseCustomCoursor
    {
        private Texture2D m_coursor;
        public MouseCustomCoursor(Game game)
        {
            m_coursor = game.Content.Load<Texture2D>(AuxillaryFunctions.Functions().ContentPathCreator() + "\\Menu\\pcoursor");
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDeviceManager graphics)
        {
            MouseState mouseState = Mouse.GetState();
            spriteBatch.Draw(m_coursor,
                new Rectangle(mouseState.X-m_coursor.Width/4, mouseState.Y-m_coursor.Height/4, m_coursor.Width, m_coursor.Height),
                null,
                Color.White,
                0,
                Vector2.Zero,
                SpriteEffects.None,
                0);
        }
    }
}
