﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageDog.Auxiliary
{
    public class AuxillaryFunctions
    {
        private static readonly  AuxillaryFunctions aux = new AuxillaryFunctions();
        private AuxillaryFunctions() { }

        public static AuxillaryFunctions Functions()
        {
            return aux;
        }

        //Did this function because for seome reason the Location in Content.Load<Texture2D> is not correct.
        public string ContentPathCreator()
        {
            string root = this.GetType().Assembly.Location;
            StringBuilder tempString = new StringBuilder();
            string[] chips = root.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < chips.Length - 1; i++)
            {
                tempString.Append(chips[i] + "\\");
            }

            tempString.Append("Content");
            return tempString.ToString();
        }

        public bool ParseIsCollidable(string isCollidable)
        {
            if (String.IsNullOrWhiteSpace(isCollidable))
            {
                return false;
            }

            bool ret;
            bool isBool = Boolean.TryParse(isCollidable, out ret);
            if (isBool)
            {
                return ret;
            }

            return false;
        }

        public float MyParseFloat(string delay)
        {
            if (String.IsNullOrWhiteSpace(delay))
            {
                return 0.0f;
            }

            float ret;
            bool isNumeric = float.TryParse(delay, out ret);
            if (isNumeric)
            {
                return ret;
            }

            return 0.0f;
        }

        public string ParseTypeToLink(string type)
        {
            if (String.IsNullOrWhiteSpace(type))
            {
                return "Obstacles\\Tree"; //Tree by default
            }
            return "Obstacles\\" + type;
        }
    }
}
