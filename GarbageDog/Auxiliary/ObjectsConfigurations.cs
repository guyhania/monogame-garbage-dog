using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageDog.Auxiliary
{
    public static class ObjectsConfigurations
    {
        public static ObjectsConfiguration GetObjectConfiguration(ObstacleTypes.ObstacleType type)
        {
            switch (type)
            {
                case ObstacleTypes.ObstacleType.Tree_A:
                    return new ObjectsConfiguration(6.0f, -0.90f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Tree_B:
                    return new ObjectsConfiguration(6.0f, -0.90f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Tree_C:
                    return new ObjectsConfiguration(6.0f, -0.90f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Bush_A:
                    return new ObjectsConfiguration(6.0f, -1.0f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Bush_B:
                    return new ObjectsConfiguration(6.0f, -1.0f, 0.0f, -2.5f);
                case ObstacleTypes.ObstacleType.Bone:
                    return new ObjectsConfiguration(2.0f, -2.5f, 0.0f, 5.0f);
                case ObstacleTypes.ObstacleType.Branch:
                    return new ObjectsConfiguration(1.5f, -2.95f, 0.0f, 175.8f);
                case ObstacleTypes.ObstacleType.Cigarette:
                    return new ObjectsConfiguration(1.0f, -2.685f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.FishBone:
                    return new ObjectsConfiguration(1.5f, -2.95f, -70.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Flea:
                    return new ObjectsConfiguration(1.0f, -2.56f, 0.0f, -10.0f);
                case ObstacleTypes.ObstacleType.Hydrant:
                    return new ObjectsConfiguration(3.0f, -2.42f, 0.0f,180.0f, -2.5f);
                case ObstacleTypes.ObstacleType.Leaf:
                    return new ObjectsConfiguration(1.0f, -2.85f, -70.0f, -2.5f);
                case ObstacleTypes.ObstacleType.LightPole:
                    return new ObjectsConfiguration(5.0f, -1.35f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Cone:
                    return new ObjectsConfiguration(1.2f, -2.52f, -30.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Poo:
                    return new ObjectsConfiguration(0.75f, -2.65f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.RoadKill:
                    return new ObjectsConfiguration(2.0f, -2.65f, -40.0f, 0.0f);
                case ObstacleTypes.ObstacleType.StopSign:
                    return new ObjectsConfiguration(5.0f, -1.58f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.Tick:
                    return new ObjectsConfiguration(1.0f, -2.55f, 0.0f, -12.0f);
                case ObstacleTypes.ObstacleType.Rock:
                    return new ObjectsConfiguration(1.0f, -2.5f, 0.0f, 0.0f);
                case ObstacleTypes.ObstacleType.TrashCan:
                    return new ObjectsConfiguration(2.0f, -2.44f, 0.0f, -6.0f);

                case ObstacleTypes.ObstacleType.Cat:
                    return new ObjectsConfiguration(2.0f,-2.85f,-55.0f,0f);
                default:
                    return null;
            }
        }
    }

    public class ObjectsConfiguration
    {
        public float m_magnification { get; }
        public float m_yValue { get; }
        public float m_xRotation { get; }
        public float m_yRotation { get; }
        public float m_zRotation { get; }

        public ObjectsConfiguration(float magnification, float yValue, float xRotation,float yRotation, float zRotation)
        {
            m_magnification = magnification;
            m_yValue = yValue;
            m_xRotation = xRotation;
            m_yRotation = yRotation;
            m_zRotation = zRotation;
        }
        public ObjectsConfiguration(float magnification, float yValue, float xRotation, float zRotation)
        {
            m_magnification = magnification;
            m_yValue = yValue;
            m_xRotation = xRotation;
            m_yRotation = 0;
            m_zRotation = zRotation;
        }
        public ObjectsConfiguration()
        {
            m_magnification = 1;
            m_yValue = 0;
            m_xRotation = 0;
            m_yRotation = 0;
            m_zRotation = 0;
        }
    }
}