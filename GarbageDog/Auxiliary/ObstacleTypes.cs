﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageDog.Auxiliary
{
    public static class ObstacleTypes
    {
        public enum ObstacleType
        {
            Tree_A,
            Tree_B,
            Tree_C,
            Bush_A,
            Bush_B,
            Bone,
            Branch,
            Cigarette,
            FishBone,
            Flea,
            Hydrant,
            Leaf,
            LightPole,
            Cone,
            Poo,
            RoadKill,
            StopSign,
            Tick,
            Rock,
            Cat,
            TrashCan

        }
    }
}
