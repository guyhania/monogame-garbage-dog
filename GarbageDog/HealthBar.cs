﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XELibrary;

namespace GarbageDog
{
    class HealthBar
    {

        public int health;
        private Texture2D healthTextureGreen;
        private Texture2D healthTextureYellow;
        private Texture2D healthTextureRed;
        private Rectangle healthRectangle;
        private Vector2 position;



        public HealthBar(Game game)
        {

            health = 540;
            healthTextureGreen = game.Content.Load<Texture2D>(@"Backgrounds\green");
            healthTextureYellow = game.Content.Load<Texture2D>(@"Backgrounds\Yellow");
            healthTextureRed = game.Content.Load<Texture2D>(@"Backgrounds\red");
            position = new Vector2(50, 100);

        }


        public void Update(int health)
        {
            healthRectangle = new Rectangle(0, 0, health, 90);
            System.Diagnostics.Debug.WriteLine("Health is : " + health.ToString());

        }

        public void Draw(GameTime gameTime, GraphicsDevice graphicsDevice, SpriteBatch spritebatch)
        {
            //spritebatch.Begin();
            if (health == 540)
            {
                spritebatch.Draw(healthTextureGreen, healthRectangle, Color.White);
            }
            if (health == 360)
            {
                spritebatch.Draw(healthTextureYellow, healthRectangle, Color.White);
            }
            if (health == 180)
            {
                spritebatch.Draw(healthTextureRed, healthRectangle, Color.White);
            }
           // spritebatch.End();
        }

        public void setHealth(int amount)
        {
            health = amount;
        }

    }
}
