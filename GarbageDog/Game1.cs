﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace GarbageDog
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {

        GraphicsDeviceManager m_graphics;
        GraphicsDeviceManager m_tgraphics;
        SpriteBatch m_spriteBatch;
        SpriteBatch m_tspriteBatch;
        private Camera m_camera;


        public MainMenu m_mainMenu;
        private TheGame m_theGame;

        

        public Game1()
        {
            m_graphics = new GraphicsDeviceManager(this);

            m_camera = new PerspectiveCamera(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            m_mainMenu = new MainMenu(this, m_graphics);
            m_theGame = new TheGame(this, m_graphics);
            InitializeGraphics();

            base.Initialize();

        }
        private void InitializeGraphics()
        {
            m_graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            m_graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            m_graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            m_graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            m_graphics.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            m_graphics.ApplyChanges();
            m_tgraphics = m_graphics;
        }
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            m_spriteBatch = new SpriteBatch(GraphicsDevice);
            m_tspriteBatch = m_spriteBatch;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {

            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            if (m_mainMenu.gameState == Menu.MainMenu)
            {
                m_mainMenu.Update(gameTime,this, Keyboard.GetState());
            }

            if (m_mainMenu.gameState == Menu.Game)
            {
                if (!m_theGame.IsLoaded())
                {
                    if (m_graphics.GraphicsProfile != GraphicsProfile.HiDef)
                    {
                        m_graphics.GraphicsProfile = GraphicsProfile.HiDef;
                        m_graphics.ApplyChanges();
                    }

                    m_theGame.LoadContent(this);
                    m_theGame.Initialize(this, m_graphics);

                }
                else
                {
                    m_theGame.Update(gameTime, Keyboard.GetState());

                    if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        Exit();


                    }
                }
            }

            base.Update(gameTime);
        }

        private void Reveent()
        {
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            if (m_mainMenu.gameState == Menu.MainMenu)
            {
                if (null == m_spriteBatch.GraphicsDevice)
                {
                    m_spriteBatch = new SpriteBatch(GraphicsDevice);
                }
                m_spriteBatch.Begin();

                m_mainMenu.Draw(gameTime, ref m_graphics, m_spriteBatch, this);

                m_spriteBatch.End();
            }

            if (m_mainMenu.gameState == Menu.Game && m_theGame.IsLoaded())
            {
                if (null == m_spriteBatch.GraphicsDevice)
                {
                    m_spriteBatch = new SpriteBatch(GraphicsDevice);
                }

                m_spriteBatch.Begin();
                m_theGame.Draw(gameTime,this,ref m_graphics,m_spriteBatch);
                m_spriteBatch.End();
            }

            base.Draw(gameTime);

        }

    }
}
